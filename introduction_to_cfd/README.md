# Introdution to CFD readme

**Disclaimer: under construction**

This folder contains notebooks introducing CFD (Computationnal Fluid Dynamics) basic tools and solvers used at CERFACS. They can be used by newcomers to start learning CFD.


## Barbatruc

People starting CFD can see examples of modules created at Cerfacs such as `barbatruc`.  This module is the first element of a work in progress, it is a CFD package dedicated to training using a light-weight solver which can simulate a Poiseuille 2D flow. 

**List:**

- **[Poiseuille.ipynb](Poiseuille.ipynb)** 