# Notebooks

This repository hosts several Jupyter notebooks developed at Cerfacs.

The Jupyter python package allows you to create notebooks with live code, visualizations and text. It is an open source web application.

[jupyter documentation](https://jupyter.org/)

## Installation

In a virtual environment:

	pip install jupyter

Go to the folder where your file **.ipynb** is and use the following command:

	jupyter notebook

A web page should open with the different notebooks.


## Links to the notebooks folder

The [programming_sessions](programming_sessions) notebooks are for python newcomers. They will provide descriptions of python, of the different python tools, of physic problems and of code error and validation.

The [spectral_analysis](spectral_analysis) notebooks are about spectral analysis and its tools such as Fast Fourier Transform, spectrum estimator and mean convergence.

The [introduction\_to_cfd](introduction_to_cfd) notebooks are Computational Fluid Dynamics tools and solvers such as barbatruc. 

## Warning

*Beware of NOT uploading large files in this repository.
No newline at end of file.*
