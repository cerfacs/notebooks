# Programming sessions readme

This folder contains notebooks introducing Python's basic and some tools used at CERFACS. They can be used by newcomers to start learning Python.


## Getting STarted

You can learn the **basic** about python even if you have never heard of it.

You are expected to fill the blank in the code. It will help you to better understand what you are doing.
Moreover, some video links will be provided for additional resources

**List:**

- If you have never used python, start with the following notebook **[python\_getting\_started](python_getting_started.ipynb)** 
- **[numpy\_getting_started.ipynb](numpy_getting_started.ipynb)** is a notebook to discover the numpy tool to handle array. Requirement: **[tools\_numpy.ipynb](tools_numpy.ipynb)**
- **[testing\_python\_and\_numpy\_basic_skills.ipynb](testing_python_and_numpy_basic_skills.ipynb)** promote simple usage of python and numpy to solve small problems. Requirement: **[tools\_numpy.ipynb](tools_numpy.ipynb)**
- A small introduction to the usage of classes, using a guitar **[classes\_and\_guitars](classes_and_guitars.ipynb)**.

## Tools notebook

You will learn to use python tools such as numpy, pandas and matplotlib thanks to the following notebooks:

- **[tools\_numpy.ipynb](tools_numpy.ipynb)** explain you how to use arrays
- **[tools\_pandas.ipynb](tools_pandas.ipynb)** is for data analysis 
- **[tools\_matplotlib.ipynb](tools_matplotlib.ipynb)** is to create figures and graphs in python


## Physics

One can also see examples of modules created at Cerfacs. 
You will learn python functionalities while studying physics such as ballistic equations, heat equation, a pollution example... And, you will learn how to define an engineering problem.

**List:**

- The **[onedimensionalheatequation.ipynb](onedimensionalheatequation.ipynb)**  notebook let us know how to solve a one dimensional heat equation thanks to python
- The **[MyFirstModuleOnBallistics.ipynb](MyFirstModuleOnBallistics.ipynb)** notebook can help someone to learn how to solve a problem from scratch, for example a ballistic movement resolution
- The **[numpy_forest_fires.ipynb](numpy_forest_fires.ipynb)** is a cellular automata modeling of forest fires. A nice playground to learn how to handle 2-D numpy arrays.
- **[village\_pollution.ipynb](village_pollution.ipynb)** is about pollution modeling
- **[defining\_the\_engineering_problem.ipynb](defining_the_engineering_problem.ipynb)** is about defining an engineering model from scratch

## Code error and validation

You can see how to handle code errors and how to validate a code. 

It is very interesting to learn how to solve code errors. It allows to understand what you can do and what you can not do. And above all, one can distinguish the trivial errors (typo) from the others.

List:

- **[python\_errors.ipynb](python_errors.ipynb)** gives a serie of codes with mistakes inside, you will simply need to fix all the mistakes...
- **[ErrorsAndValidation.ipynb](ErrorsAndValidation.ipynb)** gives small parts of code with errors, you will have to find and handle them by yourself
- **[CodeQuality.ipynb](CodeQuality.ipynb)** is associated with **linting** which shows syntactical and stylistic problems in your Python source code to solve your errors


