
"""
This module is the background for our CERFACS-COOP training "about objects".

Use it at your convenience.
I used a lot this interactive site to get the frequencies of my notes: https://muted.io/note-frequencies/
You may need to pip install numpy and sounddevice to play.
"""

import numpy as np                     # numpy, the convenient array handler
from sounddevice import play as sdplay # library to play numpy array as sound
from random import random              # used in karplus-strong guitar pluck synthetizer
from abc import ABC                    # used for object inheritance


SAMPLING_FREQ = 44100
SEMITONE_RATIO = 256./243 
        

# LOW LEVEL FUNCTIONS ======================

def pure_beep(
    amp: float = 1.0, freq: int = 440, duration: float = 1.
) -> np.array:
    """
    Generates a pure beep waveform with a single frequency tone.

    Parameters:
        amp (float, optional): The amplitude of the waveform. Defaults to 1.0.
        freq (int, optional): The frequency of the tone in Hz. Defaults to 440.
        duration (int, optional): The duration of the generated waveform in seconds. Defaults to 1.

    Returns:
        np.array: An array of generated waveform samples.

    """
    t = np.linspace(0, duration, int(duration * SAMPLING_FREQ), False)
    note = amp * np.sin(freq * t * 2 * np.pi)
    return note

def fat_beep(
    amp: float = 1.0,
    freq: int = 440,
    duration: float = 1,
    harms=12,
    decay=0.2,
) -> np.array:
    """
    Generates a fat beep waveform with harmonics using additive synthesis.

    Parameters:
        amp (float, optional): The amplitude of the fundamental frequency. Defaults to 1.0.
        freq (int, optional): The frequency of the fundamental tone in Hz. Defaults to 440.
        duration (float, optional): The duration of the generated waveform in seconds. Defaults to 1.
        harms (int, optional): The number of harmonics to include in the waveform. Defaults to 12.
        decay (float, optional): The decay factor applied to each harmonic amplitude. Defaults to 0.1.

    Returns:
        np.array: An array of generated waveform samples.

    """
    t = np.linspace(0, duration, int(duration * SAMPLING_FREQ), False)
    note = amp * np.sin(freq * t * 2 * np.pi)
    for i in range(harms):
        amp *= decay
        note += np.sin((i + 1) * freq * t * 2 * np.pi)
    return note


def kpst_pluck(freq: int, duration: float=1.0) -> np.array:
    """
    Generates a plucked string waveform using the Karplus-Strong algorithm.

    Parameters:
        duration (float): The duration of the generated waveform in seconds.
        freq (int): The frequency of the plucked string in Hz.
      
    Returns:
        np.array: An array of generated waveform samples.

    """
    resampled_sampling_frequency = int(SAMPLING_FREQ * (2.0 / duration))
    wavelength_in_samples = int(
        round((SAMPLING_FREQ / freq) * (resampled_sampling_frequency / SAMPLING_FREQ))
    )

    noise_samples = [random() * 2 - 1 for _ in range(wavelength_in_samples)]

    output_samples = []

    for _ in range(SAMPLING_FREQ * 2 // len(noise_samples)):
        for output_position in range(len(noise_samples)):
            wavetable_position = output_position % len(noise_samples)
            noise_samples[wavetable_position] = (
                noise_samples[wavetable_position]
                + noise_samples[(wavetable_position - 1) % len(noise_samples)]
            ) / 2

        output_samples += noise_samples

    exp_size = int(duration*SAMPLING_FREQ)
    output=np.zeros(exp_size)
    for i,val in enumerate(output_samples):
        try:
            output[i]=val
        except IndexError:
            pass
    
    return output


def create_base(duration: float) -> np.array:
    """
    Create the base sampling array for a song.

    Parameters:
        duration (float): The duration of the base sampling array in seconds.

    Returns:
        np.array: A numpy array of zeros representing the base sampling array.

    """
    return np.zeros(int(duration * SAMPLING_FREQ))


def add_note(base: np.array, note: np.array, start: float) -> np.array:
    """
    Add a note to the base sampling array for a song.

    Parameters:
        base (np.array): The base sampling array.
        note (np.array): The array representing the note to be added.
        start (float): The start time of the note in seconds.

    Returns:
        np.array: A numpy array representing the updated base sampling array with the added note.

    """
    id_start = int(start * SAMPLING_FREQ)

    base_out = base.copy()
    if id_start >= base.size:
        return base_out
    id_end = min(id_start + note.size, base.size)
    base_out[id_start:id_end] += note[0: id_end - id_start]

    return base_out


# DEDICATED FUNCTION ======================


def guitar_string(fret: int, tuning: float = 82.41, duration: float = 2.0) -> np.ndarray:
    """
    Generate a plucked guitar string sound.

    Parameters:
    - fret (int): The fret number to play on the guitar string.
    - tuning (float): The frequency of the string's initial tuning (default: 82.41 Hz for E2).
    - duration (float): The duration of the generated sound in seconds (default: 2.0 seconds).
    
    Returns:
    - np.ndarray: An array representing the generated sound.
    """
    # Calculate the frequency based on the tuning and fret number
    string_freq = tuning * (SEMITONE_RATIO ** fret)
    
    # Generate the plucked sound using kpst_pluck function (assuming it exists)
    return kpst_pluck(freq=string_freq, duration=duration)
    #return fat_beep(freq=string_freq, duration=duration)


# DEDICATED OBJECT ======================

class GuitarString:
    def __init__(self, tuning: float = 82.41):
        self.tuning=tuning  

    def pluck(self,fret:int, duration:float=2.)-> np.array:
        return guitar_string(fret=fret,tuning=self.tuning, duration=duration)
        

# COMPOSED OBJECT ======================

class Guitar:
    def __init__(self,strings_tunings:list):
        self.st=[]
        for tuning in strings_tunings:
            self.st.append(GuitarString(tuning))
    
    def pluck(self, frets:list, duration: float):
        note=create_base(duration=duration) 
        for i,fret in  enumerate(frets):
            if fret is not None:
                note +=self.st[i].pluck(fret, duration)
        return note

    def arpg(self, frets:list, duration: float=1, delay:float=0.13):
        """I tried this to make arpeggi, not perfect tho"""
        note=create_base(duration+delay*len(frets))  
        start = 0
        ii = 0
        step = 1        
        for i in range(2*len(frets)-2):
            if frets[ii] is not None:
                note = add_note(note,self.st[ii].pluck(frets[ii], duration),start)
                start +=delay
            if ii ==len(frets)-1:
                step=-1
            ii += step
        return note

# OBJECT WITH INHERITANCE ===============

class StringInstrument(ABC):
    def __init__(self,strings_tunings:list):
        self.st=[]
        for tuning in strings_tunings:
            self.st.append(GuitarString(tuning))
    
    def pluck(self, frets:list, duration: float):
        note=create_base(duration)  
        for i,fret in  enumerate(frets):
            if fret is not None:
                note +=self.st[i].pluck(fret, duration)
        return note
    
class GuitarInstrument(StringInstrument):
    def __init__(self):
        super().__init__([82.41,110.00,146.83,196.00,246.94,329.63])

class BassInstrument(StringInstrument):
    def __init__(self):
        super().__init__([41.203,55,73.416,97.99])



### APIs  #################

#------------------------------------------
## Using the guitar_string() functional API 
## to play sevens nation army
#------------------------------------------

# # create the necessary notes
# n7 = guitar_string(fret=7, tuning=110., duration=2.)
# n10 = guitar_string(fret=10, tuning=110., duration=2.)
# n5 = guitar_string(fret=5, tuning=110., duration=2.)
# n3 = guitar_string(fret=3, tuning=110., duration=2.)
# n2 = guitar_string(fret=2, tuning=110, duration=2.)

# # create a 4.3 second long void song
# song = create_base(duration=4.3)

# # Declare the tab
# song = add_note(song, n7, 0.3)
# song = add_note(song, n7, 1.0)
# song = add_note(song, n10, 1.3)
# song = add_note(song, n7, 1.8)
# song = add_note(song, n5, 2.2)
# song = add_note(song, n3, 2.5)
# song = add_note(song, n2, 3.4)

# sdplay(song, blocking=True, samplerate=SAMPLING_FREQ)


#------------------------------------------
## Using the Guitar() object-based API 
## to play The House of The rising sun
#------------------------------------------

guitar = Guitar([82.41,110.00,146.83,196.00,246.94,329.63])

base = create_base(duration=8.)


chord_len=2.0

chord_am = guitar.pluck([None,0,2,2,1,0],chord_len)
chord_d = guitar.pluck([None, None,0,2,3,2],chord_len)
chord_f = guitar.pluck([1,3,3,2,1,1],chord_len)
chord_c = guitar.pluck([None,3,2,0,1,0],chord_len)
chord_e = guitar.pluck([0,2,2,1,0,0],chord_len)


# Arpeggios version
# chord_am = guitar.arpg([None,1,None,2,1,0],chord_len)
# chord_c = guitar.arpg([None,3,None,0,1,0],chord_len)
# chord_d = guitar.arpg([None, None,0,2,1,2],chord_len)
# chord_f = guitar.arpg([None,None,3,2,1,1],chord_len)
# chord_e = guitar.arpg([None,2,None,1,0,0],chord_len)
# chord_em = guitar.arpg([None,2,2,2,0,None],chord_len)


base = add_note(base, chord_am,0)
base = add_note(base, chord_c,1)
base = add_note(base, chord_d,2)
base = add_note(base, chord_f,3)
base = add_note(base, chord_am,4)
base = add_note(base, chord_c,5)
base = add_note(base, chord_e,6)
base = add_note(base, chord_e,7)

sdplay(base, blocking=True, samplerate=SAMPLING_FREQ)


