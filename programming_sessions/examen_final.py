import numpy as np
import matplotlib.pyplot as plt

def grad2(arr, dx, fourth=False):
    """Compute gradient using second order central difference or fourth if requested
    
    note:
      arr[:] is :     [X0,X1,X2,X3,X4,X5,X6,X7,X8,X9]
      
    Second order
      U" = 1 ( Um1 - 2 U + Up1 )
      arr[:-2] is :  [X0,X1,X2,X3,X4,X5,X6,X7]
      arr[1:-1] is :    [X1,X2,X3,X4,X5,X6,X7,X8]
      arr[2:] is :          [X2,X3,X4,X5,X6,X7,X8,X9]
      
    Fourth order
      U" = 1/12 (-Um2 + 16 Um1 - 30 U + 16 Up1 -Up2)
      arr[4:] is :               [X4,X5,X6,X7,X8,X9]
      arr[3:-1] is :           [X3,X4,X5,X6,X7,X8]
      arr[2:-2] is :        [X2,X3,X4,X5,X6,X7]
      arr[1:-3] is :     [X1,X2,X3,X4,X5,X6]
      arr[ :-4] is :  [X0,X1,X2,X3,X4,X5]
      
    
    """
   out = np.ones_like(arr)
    # Order 2
    out[1:-1]= (arr[2:] -2*arr[1:-1] +arr[:-2])/(dx**2)
    if fourth:
        # Order 4
        out[2:-2]= 1./12*(
            -arr[4:] 
            + 16*arr[3:-1] 
            -30*arr[2:-2] 
            + 16*arr[3:-1] 
            -arr[:-4]
        )/(dx**2)
    
    out[0] = 0
    out[-1] =0
    
    return out

# Parameters
length = 1,0        # Length of the string
time_steps = 200    # Number of time steps
space_steps = 100    # Number of space steps
c = 1.0             # Wave speed
dt = 0.001          # Time step
dx = length / space_steps  # Space step

# Initial conditions (pulse in the middle)
x_space = np.linspace(0,length,space_steps)
u = np.zeros( space_steps)  # Present State
u[48] = 0.2
u[49] = 0.8
u[50] = 1.0     # Add a pulse around index 50
u[51] = 0.8
u[52] = 0.2
um1 = u     # Previous State
um2 = u     # State before Previous State

# Explicit finite difference method
# Considering U" = (U-2Um1+Um2)/dt**2
# solve the wave equation 
#          d2U/dt2 = c2 d2U/dx2


for t in range(time_steps):
    um2=um1
    um1=u

plt.plot(x_space,u, label=f'Time Step {t}')
    
    
plt.title('Wave Equation Simulation')
plt.xlabel('Position')
plt.ylabel('Amplitude')
plt.legend()
plt.show()