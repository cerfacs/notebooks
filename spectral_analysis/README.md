# Signal Analysis Readme

**Disclaimer: under construction**

The jupyter notebooks in this folder are about spectrum analysis methods.

# Fast Fourier Transform

Fast Fourier Transform (FFT) is one of the main tool for spectrum analysis. 
Therefore, it will be a good idea to do the following notebooks if you just started in this domain:

* **[1-FFT.ipynb](1-FFT.ipynb)** will help you getting started with the FFT
* **[2-FFT\_pitfalls\_and\_signal_duration.ipynb](2-FFT_pitfalls_and_signal_duration.ipynb)** is a follow up of the above notebook, to have more specific details: you will learn about pitfalls and tips for the FFT such as aliasing and signal duration.
* **[3-FFT_spectrum_variations.ipynb](3-FFT_spectrum_variations.ipynb)**, you will learn about spectrum estimator wanted properties when it is exposed to some variations:
    - signal length variation
    - amplitude variation
    - noise variation
    - signal sampling variation 
    - zero padding


# Confidence in Averaging

The mean value from experimental data might not be the exact mean value. The confidence interval allows to calculate the probability of the exact value being in this range. For further information, see the following notebook: **[Mean_convergence.ipynb](Mean_convergence.ipynb)**



